function addAnimeteCss() {
  var link = document.createElement( "link" );
  link.href = "https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css";
  link.type = "text/css";
  link.rel = "stylesheet";

  document.getElementsByTagName( "head" )[0].appendChild( link );
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

addAnimeteCss();

const animations = ['rubberBand', 'shake', 'flip'];

window.setInterval(
    () => document.getElementById('hplogo').className='animated '+ animations[getRandomInt(0,3)],
    10000
);


